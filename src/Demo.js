import React, {Component} from 'react';

class Demo extends Component {

    constructor(props){
        super(props);

        this.state={
         list:[],
         current:''
        };
    }

    txtChange = (e) => {
        let vlu = e.target.value;
        this.setState({
            current:vlu  
        })
    }

     addEdition = () =>{
    let edition = this.state.current;
   
    if(edition != ''){
      this.setState({
        list:[...this.state.list,edition] 
      })
      this.setState({
          current:''
      });
      alert(`${edition} added`);
    }else{
        alert('add eddition name');
    }
    }

    deleteHandler = (k) => {
        let ar = this.state.list;
        ar.splice(k,1);
        this.setState({
            list:ar
        })
    }

    render(){
        return(
     <div>
         <h1>Add Editions</h1>
         <input type="text" onChange={this.txtChange} value={this.state.current}/>
         <button onClick={this.addEdition}>Add</button>

         <div>
             <h2>Editions List</h2>
             <ul>
                 {this.state.list.map((itm,indx)=>{
                 return(<li>{itm}<button onClick={() => this.deleteHandler(indx)}>Delete</button></li>)
                 })}
                 
             </ul>
         </div>
     </div>

        );
    }
}

export default Demo;