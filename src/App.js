
import "./App.css";
import React from "react";
import { Header, Footer } from "./Home";
import Contact from "./Contact";
import Demo from './Demo';

function App() {
  

  return (
    <div className="App">
      <Header />

      <div style={{minHeight:500}}>
      <Demo/>
      </div>
      <Footer />
    </div>
  );
}

export default App;
